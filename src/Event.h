//
// Created by bantdit on 10/30/17.
//

#ifndef EVENT_H
#define EVENT_H

#include "EventReceivable.h"
#include <algorithm>
#include <boost/functional/hash.hpp>
#include <functional>
#include <unordered_map>
#include <utility>

template<typename... Args>
class Event
{
private:
    struct Dummy
    {
        void func(){};
        virtual void vFunc(){};
    };

    using EventHandlerIdentifier =
      std::array<std::byte, sizeof(uint_fast64_t) + std::max(sizeof(&Dummy::func), sizeof(&Dummy::vFunc))>;

public:
    class EventHandler
    {
        friend Event;

    public:
        template<class T, class M>
        EventHandler(T* instance, void (M::*member)(Args...))
          : isStatic_{ false }
          , identifier_{}
          , eventReceiver_{ instance->eventReceiver() }
          , handler_{}
        {
            auto id = instance->id();

            std::memset(identifier_.data(), 0, identifier_.size());
            std::memcpy(identifier_.data(), &id, sizeof(uint_fast64_t));
            std::memcpy(identifier_.data() + sizeof(uint_fast64_t), &member, sizeof(member));

            auto eventReceiver = eventReceiver_;

            handler_ = [eventReceiver, member](auto&&... args) -> void {
                assert(!eventReceiver.expired());

                auto receiver = eventReceiver.lock();

                ((static_cast<T*>(receiver->instance()))->*member)(std::forward<decltype(args)>(args)...);
            };
        }

        template<typename SFunc>
        EventHandler(SFunc const& staticHandler)
          : isStatic_{ true }
          , identifier_{}
          , eventReceiver_{}
          , handler_{ [staticHandler](auto&&... args) -> void {
              staticHandler(std::forward<decltype(args)>(args)...);
          } }
        {
            std::memset(identifier_.data(), 0, identifier_.size());
            std::memcpy(identifier_.data() + sizeof(uint_fast64_t), &staticHandler, sizeof(uintptr_t));
        }

        EventHandler(EventHandler const&) = default;

        EventHandler(EventHandler&&) = default;

        ~EventHandler() = default;

    public:
        EventHandler& operator=(EventHandler const&) = default;

        EventHandler& operator=(EventHandler&&) = default;

        template<typename... EventArgs>
        void operator()(EventArgs&&... args)
        {
            invoke(std::forward<EventArgs>(args)...);
        }

        explicit operator bool() const { return isStatic_ ? true : !eventReceiver_.expired(); }

    public:
        template<typename... EventArgs>
        void invoke(EventArgs&&... args)
        {
            handler_(std::forward<EventArgs>(args)...);
        }

    public:
        auto identifier() const -> EventHandlerIdentifier const& { return identifier_; }

    private:
        bool isStatic_;

        EventHandlerIdentifier identifier_;

        std::weak_ptr<EventReceivable::EventReceiver> eventReceiver_;

        std::function<void(Args...)> handler_;
    };

public:
    Event()
      : eventHandlers_{}
    {
    }

    Event(Event const&) = delete;

    Event(Event&&) = default;

    ~Event() = default;

public:
    Event& operator=(Event const&) = delete;

    Event& operator=(Event&&) = default;

    template<typename Handler>
    auto operator+=(Handler const& rhs) -> EventHandler
    {
        static_assert(std::is_same_v<EventHandler, std::decay_t<Handler>> || std::is_invocable_v<Handler, Args...>);

        if constexpr (std::is_same_v<EventHandler, std::decay_t<Handler>>) {
            eventHandlers_.emplace(rhs.identifier(), rhs);
            return rhs;
        } else {
            EventHandler eh{ rhs };
            eventHandlers_.emplace(eh.identifier(), eh);
            return eh;
        }
    }

    void operator-=(EventHandler const& rhs) { eventHandlers_.erase(rhs.identifier()); }

    template<typename... EventArgs>
    void operator()(EventArgs&&... args)
    {
        auto it = eventHandlers_.begin();

        while (it != eventHandlers_.end()) {
            EventHandler& eventHandler = (*it).second;

            if (eventHandler) {
                eventHandler(std::forward<EventArgs>(args)...);
                it++;
            } else {
                it = eventHandlers_.erase(it);
            }
        }
    }

private:
    std::unordered_map<EventHandlerIdentifier, EventHandler, boost::hash<EventHandlerIdentifier>> eventHandlers_;
};
#endif // PROJECT_EVENT_H
