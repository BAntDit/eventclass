//
// Created by bantdit on 10/31/17.
//

#include <iostream>
#include "../src/Event.h"

class Emitter
{
public:
    Emitter() = default;
    ~Emitter() = default;

public:
    Event<uint32_t> testEvent;
};

class Receiver final: public EventReceivable
{
private:
    uint32_t data_;

public:
    Receiver() : data_{0} { }

    ~Receiver() = default;

public:
    void onTestEvent(uint32_t val) { data_ = val; }

    auto data() const -> uint32_t { return data_; }
};

int main() {
    Emitter emitter;

    Receiver receiver1;

    std::cout << "receiver1, before event: " << receiver1.data() << std::endl;

    emitter.testEvent += Event<uint32_t>::EventHandler(&receiver1, &Receiver::onTestEvent);

    {
        Receiver receiver2;

        std::cout << "receiver2, before event: " << receiver2.data() << std::endl;

        emitter.testEvent += Event<uint32_t>::EventHandler(&receiver2, &Receiver::onTestEvent);

        emitter.testEvent(5);

        std::cout << "receiver1, after first event: " << receiver1.data() << std::endl;
        std::cout << "receiver2, after first event: " << receiver2.data() << std::endl;

        // emitter.testEvent -= Event<uint32_t>::EventHandler(&receiver2, &Receiver::onTestEvent); // not necessary
    }

    emitter.testEvent(6);

    std::cout << "receiver1, after second event: " << receiver1.data() << std::endl;
    std::cout << "receiver2 is no longer existen" << std::endl;

    return 0;
}
