# Summary

C++ event system

### features:

* RAII-friendly
* any number of event parameters
* ability to use member functions, free functions and lambdas as well
* almost c# like usage

# usage example

```C++
class Emitter {
public:
	Event<uint32_t> event;
}

class Receiver final: public EventReceivable {
public:
	void onEvent(uint32_t val);
}

Emitter  emitter;
Receiver receiver;

emitter.event += Event<uint32_t>::EventHandler(&receiver, &Receiver::onEvent);
emitter.event(42);
```

# Dependencies

* [Boost C++ Libraries](http://www.boost.org/) - provides a set of free peer-reviewed portable C++ source libraries
* [GTest](https://github.com/google/googletest) - Google Test, Google's C++ test framework!