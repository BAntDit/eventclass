//
// Created by bantdit on 10/30/17.
//

#include <gtest/gtest.h>

#include "../src/Event.h"
#include "../src/EventReceivable.h"

class TestEmitter
{
public:
    Event<>                 noArgumentsEvent;
    Event<uint32_t>         oneArgumentsEvent;
    Event<uint32_t, bool>   twoArgumentsEvent;
    Event<uint32_t*>        pointerArgumentEvent;

public:
    TestEmitter() = default;
    ~TestEmitter() = default;
};

class NoArgsEventReceiver final: public EventReceivable
{
protected:
    bool eventResult_;

public:
    NoArgsEventReceiver() :
        eventResult_{false}
    { }

    NoArgsEventReceiver(NoArgsEventReceiver const &other) :
        EventReceivable{},
        eventResult_{other.eventResult_}
    {
    }

    NoArgsEventReceiver(NoArgsEventReceiver&&) = default;

    ~NoArgsEventReceiver() = default;

    NoArgsEventReceiver& operator=(NoArgsEventReceiver const&) = default;

    NoArgsEventReceiver& operator=(NoArgsEventReceiver&&) = default;

    void onEvent() { eventResult_ = true; };

    explicit operator bool() const { return eventResult_; }
};

class OneArgEventReceiver final: public EventReceivable
{
protected:
    uint32_t eventResult_;

public:
    OneArgEventReceiver() : eventResult_{0} { }

    ~OneArgEventReceiver() = default;

    void onEvent(uint32_t value) { eventResult_ = value; };

    auto result() const -> uint32_t const& { return eventResult_; }
};

class TowArgsEventReceiver final: public EventReceivable
{
protected:
    uint32_t    eventResult1_;
    bool        eventResult2_;

public:
    TowArgsEventReceiver() :
        eventResult1_{0}
        , eventResult2_{false}
    { }

    ~TowArgsEventReceiver() = default;

    void onEvent(uint32_t value1, bool value2) {
        eventResult1_ = value1;
        eventResult2_ = value2;
    };

    auto testResult(uint32_t testValue1, bool testValue2) const -> bool {
        return eventResult1_ == testValue1 && eventResult2_ == testValue2;
    }
};

class EventTestFixture: public testing::Test
{
protected:
    TestEmitter emitter_;

    void SetUp() override {}

    void TearDown() override {}
};

testing::AssertionResult CopyReceiversTest(TestEmitter& emitter) {
    NoArgsEventReceiver eventReceiver1;

    emitter.noArgumentsEvent += Event<>::EventHandler(&eventReceiver1, &NoArgsEventReceiver::onEvent);

    NoArgsEventReceiver eventReceiver2(eventReceiver1);

    emitter.noArgumentsEvent();

    if (!eventReceiver1 || eventReceiver2) {
        return testing::AssertionFailure();
    }

    emitter.noArgumentsEvent += Event<>::EventHandler(&eventReceiver2, &NoArgsEventReceiver::onEvent);

    emitter.noArgumentsEvent();

    return (eventReceiver1 && eventReceiver2) ? testing::AssertionSuccess() : testing::AssertionFailure();
}

testing::AssertionResult MoveReceiversTest(TestEmitter& emitter) {
    NoArgsEventReceiver eventReceiver1;

    emitter.noArgumentsEvent += Event<>::EventHandler(&eventReceiver1, &NoArgsEventReceiver::onEvent);

    NoArgsEventReceiver eventReceiver2(std::move(eventReceiver1));

    emitter.noArgumentsEvent();

    return (!eventReceiver1 && eventReceiver2) ? testing::AssertionSuccess() : testing::AssertionFailure();
}


testing::AssertionResult NoArgsEventReceivingTest(TestEmitter& emitter) {
    NoArgsEventReceiver eventReceiver;

    emitter.noArgumentsEvent += Event<>::EventHandler(&eventReceiver, &NoArgsEventReceiver::onEvent);

    emitter.noArgumentsEvent();

    return eventReceiver ? testing::AssertionSuccess() : testing::AssertionFailure();
}

testing::AssertionResult OneArgEventReceivingTest(TestEmitter& emitter) {
    const uint32_t testValue = 5;

    OneArgEventReceiver eventReceiver;

    emitter.oneArgumentsEvent +=
            Event<uint32_t>::EventHandler(&eventReceiver, &OneArgEventReceiver::onEvent);

    emitter.oneArgumentsEvent(testValue);

    return eventReceiver.result() == testValue ? testing::AssertionSuccess() : testing::AssertionFailure();
}

testing::AssertionResult TwoArgsEventReceivingTest(TestEmitter& emitter) {
    const uint32_t testValue1 = 5;
    const bool testValue2 = true;

    TowArgsEventReceiver eventReceiver;

    emitter.twoArgumentsEvent +=
            Event<uint32_t, bool>::EventHandler(&eventReceiver, &TowArgsEventReceiver::onEvent);

    emitter.twoArgumentsEvent(testValue1, testValue2);

    return eventReceiver.testResult(testValue1, testValue2) ? testing::AssertionSuccess() : testing::AssertionFailure();
}

void RAIITest(TestEmitter& emitter) {
    {
        NoArgsEventReceiver eventReceiver;

        emitter.noArgumentsEvent += Event<>::EventHandler(&eventReceiver, &NoArgsEventReceiver::onEvent);

        emitter.noArgumentsEvent();

        // DGAF about missed receiver
        // emitter.noArgumentsEvent -= Event<>::EventHandler(&eventReceiver, &NoArgsEventReceiver::onEvent);
    }

    emitter.noArgumentsEvent(); // is ok
}

testing::AssertionResult StaticHandlerTest() {
    TestEmitter emitter;

    uint32_t result = 0;

    emitter.oneArgumentsEvent += Event<uint32_t>::EventHandler([&result](uint32_t val) -> void { result = val; });

    emitter.oneArgumentsEvent(5);

    return result == 5 ? testing::AssertionSuccess() : testing::AssertionFailure();
}

testing::AssertionResult LambdaHandlerTest() {
    TestEmitter emitter;

    uint32_t result = 0;

    emitter.oneArgumentsEvent += [&result](uint32_t val) -> void { result = val; };

    emitter.oneArgumentsEvent(5);

    return result == 5 ? testing::AssertionSuccess() : testing::AssertionFailure();
}

TEST_F(EventTestFixture, NoArgsEventsTesting) {
    EXPECT_TRUE(NoArgsEventReceivingTest(emitter_));
}

TEST_F(EventTestFixture, OneArgEventTesting) {
    EXPECT_TRUE(OneArgEventReceivingTest(emitter_));
}

TEST_F(EventTestFixture, TwoArgsEventTesting) {
    EXPECT_TRUE(TwoArgsEventReceivingTest(emitter_));
}

TEST_F(EventTestFixture, CopyReceiversTesting) {
    EXPECT_TRUE(CopyReceiversTest(emitter_));
}

TEST_F(EventTestFixture, MoveReceiversTesting) {
    EXPECT_TRUE(MoveReceiversTest(emitter_));
}

TEST_F(EventTestFixture, RAIITesting) {
    EXPECT_NO_THROW(RAIITest(emitter_));
}

TEST(StaticHandlers, TestStaticHandlers) {
    EXPECT_TRUE(StaticHandlerTest());
}

TEST(LambdaHandlers, TestLambdaHandlers) {
    EXPECT_TRUE(LambdaHandlerTest());
}